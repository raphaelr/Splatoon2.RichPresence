﻿namespace Splatoon2.RichPresence
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelStarted = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textWeaponFormat = new System.Windows.Forms.TextBox();
            this.textStageFormat = new System.Windows.Forms.TextBox();
            this.buttonUpdatePresence = new System.Windows.Forms.Button();
            this.numPartySizeMax = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numPartySize = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.comboWeapon = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboStage = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboDetails = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboState = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusText = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerCallbacks = new System.Windows.Forms.Timer(this.components);
            this.panelStarted.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPartySizeMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPartySize)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelStarted
            // 
            this.panelStarted.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelStarted.Controls.Add(this.label8);
            this.panelStarted.Controls.Add(this.label1);
            this.panelStarted.Controls.Add(this.textWeaponFormat);
            this.panelStarted.Controls.Add(this.textStageFormat);
            this.panelStarted.Controls.Add(this.buttonUpdatePresence);
            this.panelStarted.Controls.Add(this.numPartySizeMax);
            this.panelStarted.Controls.Add(this.label7);
            this.panelStarted.Controls.Add(this.numPartySize);
            this.panelStarted.Controls.Add(this.label6);
            this.panelStarted.Controls.Add(this.comboWeapon);
            this.panelStarted.Controls.Add(this.label5);
            this.panelStarted.Controls.Add(this.comboStage);
            this.panelStarted.Controls.Add(this.label4);
            this.panelStarted.Controls.Add(this.comboDetails);
            this.panelStarted.Controls.Add(this.label3);
            this.panelStarted.Controls.Add(this.comboState);
            this.panelStarted.Controls.Add(this.label2);
            this.panelStarted.Enabled = false;
            this.panelStarted.Location = new System.Drawing.Point(1, 0);
            this.panelStarted.Name = "panelStarted";
            this.panelStarted.Size = new System.Drawing.Size(551, 192);
            this.panelStarted.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(315, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Text";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Image";
            // 
            // textWeaponFormat
            // 
            this.textWeaponFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textWeaponFormat.Location = new System.Drawing.Point(318, 114);
            this.textWeaponFormat.Name = "textWeaponFormat";
            this.textWeaponFormat.Size = new System.Drawing.Size(225, 20);
            this.textWeaponFormat.TabIndex = 5;
            // 
            // textStageFormat
            // 
            this.textStageFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textStageFormat.Location = new System.Drawing.Point(318, 86);
            this.textStageFormat.Name = "textStageFormat";
            this.textStageFormat.Size = new System.Drawing.Size(225, 20);
            this.textStageFormat.TabIndex = 3;
            // 
            // buttonUpdatePresence
            // 
            this.buttonUpdatePresence.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpdatePresence.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdatePresence.Location = new System.Drawing.Point(3, 166);
            this.buttonUpdatePresence.Name = "buttonUpdatePresence";
            this.buttonUpdatePresence.Size = new System.Drawing.Size(539, 23);
            this.buttonUpdatePresence.TabIndex = 8;
            this.buttonUpdatePresence.Text = "Update Presence";
            this.buttonUpdatePresence.UseVisualStyleBackColor = true;
            this.buttonUpdatePresence.Click += new System.EventHandler(this.buttonUpdatePresence_Click);
            // 
            // numPartySizeMax
            // 
            this.numPartySizeMax.Location = new System.Drawing.Point(144, 140);
            this.numPartySizeMax.Name = "numPartySizeMax";
            this.numPartySizeMax.Size = new System.Drawing.Size(43, 20);
            this.numPartySizeMax.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(126, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "/";
            // 
            // numPartySize
            // 
            this.numPartySize.Location = new System.Drawing.Point(77, 140);
            this.numPartySize.Name = "numPartySize";
            this.numPartySize.Size = new System.Drawing.Size(43, 20);
            this.numPartySize.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Party size:";
            // 
            // comboWeapon
            // 
            this.comboWeapon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboWeapon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboWeapon.FormattingEnabled = true;
            this.comboWeapon.Location = new System.Drawing.Point(77, 113);
            this.comboWeapon.Name = "comboWeapon";
            this.comboWeapon.Size = new System.Drawing.Size(235, 21);
            this.comboWeapon.TabIndex = 4;
            this.comboWeapon.SelectedIndexChanged += new System.EventHandler(this.comboWeapon_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Weapon:";
            // 
            // comboStage
            // 
            this.comboStage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboStage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStage.FormattingEnabled = true;
            this.comboStage.Location = new System.Drawing.Point(77, 86);
            this.comboStage.Name = "comboStage";
            this.comboStage.Size = new System.Drawing.Size(235, 21);
            this.comboStage.TabIndex = 2;
            this.comboStage.SelectedIndexChanged += new System.EventHandler(this.comboStage_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Stage:";
            // 
            // comboDetails
            // 
            this.comboDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboDetails.FormattingEnabled = true;
            this.comboDetails.Location = new System.Drawing.Point(77, 14);
            this.comboDetails.Name = "comboDetails";
            this.comboDetails.Size = new System.Drawing.Size(466, 21);
            this.comboDetails.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Details:";
            // 
            // comboState
            // 
            this.comboState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboState.FormattingEnabled = true;
            this.comboState.Location = new System.Drawing.Point(77, 41);
            this.comboState.Name = "comboState";
            this.comboState.Size = new System.Drawing.Size(466, 21);
            this.comboState.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "State:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusText});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.statusStrip1.Location = new System.Drawing.Point(0, 197);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(552, 20);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusText
            // 
            this.statusText.Name = "statusText";
            this.statusText.Size = new System.Drawing.Size(26, 15);
            this.statusText.Text = "Idle";
            // 
            // timerCallbacks
            // 
            this.timerCallbacks.Interval = 1000;
            this.timerCallbacks.Tick += new System.EventHandler(this.timerCallbacks_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 217);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panelStarted);
            this.Name = "MainForm";
            this.Text = "Splatoon 2";
            this.panelStarted.ResumeLayout(false);
            this.panelStarted.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPartySizeMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPartySize)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelStarted;
        private System.Windows.Forms.ComboBox comboDetails;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboWeapon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboStage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numPartySize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numPartySizeMax;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonUpdatePresence;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusText;
        private System.Windows.Forms.Timer timerCallbacks;
        private System.Windows.Forms.TextBox textWeaponFormat;
        private System.Windows.Forms.TextBox textStageFormat;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
    }
}

