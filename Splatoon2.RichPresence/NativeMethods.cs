﻿using System;
using System.Runtime.InteropServices;

namespace Splatoon2.RichPresence
{
    internal static class NativeMethods
    {
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ReadyHandler();
        [UnmanagedFunctionPointer(CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public delegate void DisconnectedHandler(int errorCode, string message);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public delegate void ErroredHandler(int errorCode, string message);

        [DllImport("discord-rpc.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void Discord_Initialize(string applicationId, ref DiscordEventHandlers handlers,
            int autoRegister, string steamId);

        [DllImport("discord-rpc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Discord_Shutdown();

        [DllImport("discord-rpc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Discord_RunCallbacks();

        [DllImport("discord-rpc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Discord_UpdatePresence(ref DiscordRichPresence presence);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct DiscordRichPresence
        {
            public IntPtr state; /* max 128 bytes */
            public IntPtr details; /* max 128 bytes */
            public long startTimestamp;
            public long endTimestamp;
            public string largeImageKey; /* max 32 bytes */
            public IntPtr largeImageText; /* max 128 bytes */
            public string smallImageKey; /* max 32 bytes */
            public IntPtr smallImageText; /* max 128 bytes */
            public string partyId; /* max 128 bytes */
            public int partySize;
            public int partyMax;
            public string matchSecret; /* max 128 bytes */
            public string joinSecret; /* max 128 bytes */
            public string spectateSecret; /* max 128 bytes */
            public byte instance;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DiscordEventHandlers
        {
            public ReadyHandler Ready;
            public DisconnectedHandler Disconnected;
            public ErroredHandler Errored;
            public IntPtr JoinGame;
            public IntPtr SpectateGame;
            public IntPtr JoinRequest;
        }
    }
}