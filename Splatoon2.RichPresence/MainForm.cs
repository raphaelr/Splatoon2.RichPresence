﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Splatoon2.RichPresence
{
    public partial class MainForm : Form
    {
        private readonly AppSettings _settings;
        private NativeMethods.DiscordEventHandlers _handlers;

        public MainForm()
        {
            InitializeComponent();
            _settings = AppSettings.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.conf"));
            comboDetails.Items.AddRange(_settings.Details.Cast<object>().ToArray());
            comboState.Items.AddRange(_settings.States.Cast<object>().ToArray());
            comboStage.Items.AddRange(_settings.LargeImages.Cast<object>().ToArray());
            comboWeapon.Items.AddRange(_settings.SmallImages.Cast<object>().ToArray());
            comboDetails.SelectedIndex = comboState.SelectedIndex = comboStage.SelectedIndex = comboWeapon.SelectedIndex = 0;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Log(0, "Initializing...");
            _handlers.Ready += DiscordOnReady;
            _handlers.Disconnected += DiscordOnDisconnected;
            _handlers.Errored += DiscordOnErrored;
            NativeMethods.Discord_Initialize(_settings.AppId, ref _handlers, 1, null);

            timerCallbacks.Enabled = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            NativeMethods.Discord_Shutdown();
        }

        private void comboStage_SelectedIndexChanged(object sender, EventArgs e)
        {
            textStageFormat.Text = GetComboKey(comboStage) != null ? comboStage.SelectedItem?.ToString() : "";
        }

        private void comboWeapon_SelectedIndexChanged(object sender, EventArgs e)
        {
            textWeaponFormat.Text = GetComboKey(comboWeapon) != null ? comboWeapon.SelectedItem?.ToString() : "";
        }

        private void buttonUpdatePresence_Click(object sender, EventArgs e)
        {
            var toFree = new List<IntPtr>();
            var presence = new NativeMethods.DiscordRichPresence();
            presence.state = Encode(comboState.Text, toFree);
            presence.details = Encode(comboDetails.Text, toFree);
            presence.largeImageKey = GetComboKey(comboStage);
            if (!string.IsNullOrEmpty(textStageFormat.Text))
            {
                presence.largeImageText = Encode(textStageFormat.Text, toFree);
            }
            presence.smallImageKey = GetComboKey(comboWeapon);
            if (!string.IsNullOrEmpty(textWeaponFormat.Text))
            {
                presence.smallImageText = Encode(textWeaponFormat.Text, toFree);
            }
            presence.partySize = (int) numPartySize.Value;
            presence.partyMax = (int) numPartySizeMax.Value;

            NativeMethods.Discord_UpdatePresence(ref presence);
            foreach (var x in toFree)
            {
                Marshal.FreeCoTaskMem(x);
            }

            Log(0, $"Sent presence update {DateTime.Now}");
        }

        private void DiscordOnReady()
        {
            Log(-1, "Ready.");
            panelStarted.Enabled = true;
        }

        private void DiscordOnDisconnected(int errorcode, string message)
        {
            Log(1, $"Disconnected: {errorcode} ({message})");
            panelStarted.Enabled = false;
        }

        private void DiscordOnErrored(int errorcode, string message)
        {
            Log(1, $"Error: {errorcode} ({message})");
        }

        private void timerCallbacks_Tick(object sender, EventArgs e)
        {
            NativeMethods.Discord_RunCallbacks();
        }

        private void Log(int badness, string text)
        {
            statusText.ForeColor = badness == 0 ? Color.Black : badness < 0 ? Color.DarkGreen : Color.DarkRed;
            statusText.Text = text;
        }

        private static string GetComboKey(ComboBox combo)
        {
            var item = combo.SelectedItem as ComboItem;
            return item?.Key;
        }

        private static IntPtr Encode(string str, List<IntPtr> toFree)
        {
            var bytes = Encoding.UTF8.GetBytes(str);
            var ret = Marshal.AllocCoTaskMem(bytes.Length + 1);
            Marshal.Copy(bytes, 0, ret, bytes.Length);
            Marshal.WriteByte(ret, bytes.Length, 0);
            toFree.Add(ret);
            return ret;
        }
    }
}
