﻿using System.Collections.Generic;
using System.IO;

namespace Splatoon2.RichPresence
{
    public class AppSettings
    {
        public string AppId { get; private set; }
        public List<ComboItem> LargeImages { get; }
        public List<ComboItem> SmallImages { get; }
        public List<string> Details { get; }
        public List<string> States { get; }

        private AppSettings()
        {
            LargeImages = new List<ComboItem> {new ComboItem(null, "(None)")};
            SmallImages = new List<ComboItem> {new ComboItem(null, "(None)")};
            Details = new List<string>();
            States = new List<string>();
        }

        public static AppSettings Load(string path)
        {
            var retVal = new AppSettings();
            foreach (var line in File.ReadLines(path))
            {
                retVal.AddLine(line);
            }
            return retVal;
        }

        private void AddLine(string line)
        {
            line = (line ?? "").Trim();
            if (string.IsNullOrEmpty(line) || line.StartsWith("#"))
            {
                return;
            }
            var fields = line.Split(':');
            if (fields.Length < 2)
            {
                return;
            }

            switch (fields[0])
            {
                case "appid":
                    AppId = fields[1];
                    break;
                case "large":
                    AddComboItem(LargeImages, fields);
                    break;
                case "small":
                    AddComboItem(SmallImages, fields);
                    break;
                case "details":
                    Details.Add(fields[1]);
                    break;
                case "state":
                    States.Add(fields[1]);
                    break;
            }
        }

        private static void AddComboItem(List<ComboItem> list, string[] fields)
        {
            if (fields[1] == "-")
            {
                list.Add(new ComboItem(null, new string('-', 80)));
            }
            else if (fields.Length > 2)
            {
                list.Add(new ComboItem(fields[1], fields[2]));
            }
        }
    }

    public class ComboItem
    {
        public string Key { get; }
        public string Text { get; }

        public ComboItem(string key, string text)
        {
            Key = key;
            Text = text;
        }

        public override string ToString() => Text;
    }
}